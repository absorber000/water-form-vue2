export const roundValue = (value) => {
    const decimals = 3;
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
};
