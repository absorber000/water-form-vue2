import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Vuelidate from 'vuelidate';
import VuejsDialog from "vuejs-dialog"
import VueMask from 'v-mask';
import VuePersist from 'vue-persist';
import App from './App.vue'
import translations from "./resources/translations";

import 'vuejs-dialog/dist/vuejs-dialog.min.css';

Vue.config.formApiGet = `http://localhost:5000/${document.documentElement.lang}/waterservice/api/table`;
Vue.config.formApiPost = 'http://localhost:3000/dolphins';
//Vue.config.formFile = 'http://192.168.50.176:6456/ru/NewFiles/Upload';
Vue.config.formFile = 'http://localhost:5000/ru/NewFiles/Upload';
Vue.config.formTableRecordsPerPage = '5';
// Vue.config.formApiPostFail = 'https://www.mocky.io/v2/5ade0bf2300000272b4b29b9';

Vue.use(VueI18n);
Vue.use(Vuelidate);
Vue.use(VuePersist);
Vue.use(VueMask);
Vue.use(VuejsDialog);


Vue.directive('multi-event', {
  name: 'multi-event',
  bind: function(el, binding, vnode) {
    el.__handler__ = binding.value;
    binding.value.evt.forEach(e => el.addEventListener(e, functionWrapper))
  },
  unbind: function(el, binding) {
    el.__handler__.evt.forEach(e => el.removeEventListener(e, functionWrapper));
    el.__handler__ = null
  }
});

Vue.config.formApiUrl = process.env.FORM_API_URL;

function functionWrapper(e) {
  e.target.__handler__.fn(e)
}

const i18n = new VueI18n({
  locale: document.documentElement.lang,
  fallbackLocale: 'ru',
  messages: translations
});

new Vue({
  el: '#app',
  i18n,
  render: h => h(App)
});
