import axios from 'axios';
import * as R from 'ramda';
import Record from '../record/Record.vue';
import Vue from 'vue'
import Pagination from '../pagination/Pagination.vue';
import {roundValue} from '../../helpers/roundValue';

function isRequiredError(fieldValue) {
    return fieldValue.length === 0 || +fieldValue === 0;
}

export default {
    name: 'app-form',
    components: {
        record: Record,
        pagination: Pagination
    },
    persist: ['form'],
    data() {
        return {
            isVisible: false,
            isTotalVisible: false,
            isSubmitted: false,
            isSubmittedSuccess: false,
            isSubmittedError: false,
            isFormError: false,
            isTablePageError: false,
            isFormSubmitting: false,
            isFileUploadError: false,
            errors: [], //tableErrors
            table: {},
            recordsCount: 0,
            pageCount: 0,
            recordsPerPage: Vue.config.formTableRecordsPerPage,
            currentPage: this.getCurrentPage(),
            fileName: '',
            files: [],
            localityList: [],
            guList: [],
            repMonthList: this.getRepMonthList(),
            json: {
                Bin: "",
                Phone: "",
                GosOrganId: "",
                Month: "",
                Year: "",
                StartYearSubsidies: "",
                Table: "",
                Xml: "",
                Files: "",
                RegionId: "",
                Email: ""
            },
            sessionId: $('#sessionId').text(),
            ceuProdServiceUrl: $('#ceuProdServiceUrl').text(),
            lang: $('#lang').text(),
            iin: $('#iin').text(),
            email: $('#email').text(),
            form: {
                bin: '',
                file: '',
                orgname: '',
                headFio: '',
                contactPhone: '',
                urAddress: '',
                email: '',
                guName: '',
                locality: '',
                repMonth: '',
                repYear: '',
                subsSum: '',
                orgnameId: '',
            },
            validations: {
                bin: ['required'],
                orgname: ['required'],
                headFio: ['required'],
                contactPhone: ['required'],
                urAddress: ['required'],
                repMonth: ['required'],
                repYear: ['required'],
                email: ['required'],
                locality: ['required'],
                file: ['required'],
            },
        }
    },
    mounted() {
        this.getContragentInfo();
        this.getRegionList();
        this.setFromLocalStorage();
        this.getCurrentPageRecords(this.currentPage, true);
    },
    computed: {
        allRecords: function(){
            var all = [];
            for (let i = 0; i < this.pageCount; i++) {
                if(this.table[`page${i}`]){
                    this.table[`page${i}`].forEach((element) => {
                        all.push(element)
                    });
                }
            }
            return all
        },
        total: function () {
            var currentPageTotal = {
                peopleAmount: 0,
                peopleConsumptionRate: 0,
                subsidyConsumptionRate: 0,
                consumptionRate: 0,
                tariffPrice: 0,
                subsidySum: 0,
                total: 0,
            };

            for (var page in this.table) {
                currentPageTotal = this.table[page].reduce((obj, curr) => {
                    obj.peopleAmount += +curr.peopleAmount;
                    obj.peopleConsumptionRate += +curr.peopleConsumptionRate;
                    obj.subsidyConsumptionRate += +curr.subsidyConsumptionRate;
                    obj.consumptionRate += +curr.consumptionRate;
                    obj.tariffPrice += +curr.tariffPrice;
                    obj.subsidySum += +curr.subsidySum;
                    obj.total += +curr.total;
                    return obj;
                }, currentPageTotal);
            }

            return currentPageTotal;
        },
    },
    methods: {

        localityChange(event){
            var arr = [];
            var regionId = event.target.value;
            $.ajax({
                method: "GET",
                url: `${this.ceuProdServiceUrl}/${this.lang}/DirectoryOfContragents/ListWithServiceAndDistrict?serviceId=96&&districtId=${regionId}&&sessionId=${this.sessionId}`,
                contentType: 'application/json',
                success: (response) => {
                console.log(response);
                    response.forEach((element) => {
                        arr.push({value: element.id, label: element.name})
                    });
                },
                    error: () => {
                        showErrorDialog(this.$t('form.fail'));
                    }
                });
            this.guList = arr;
        },

        toggleTotal(){
            if(this.currentPage+1 === this.pageCount){
                this.getNotGeted().then(() => {
                    this.isTotalVisible = true;
                });
            }else{
                this.isTotalVisible = false;
            }
        },
        resetDialogOk() {
            localStorage.clear();
            window.location.reload();
        },
        setFromLocalStorage() {
            if (!localStorage.getItem("records")) {
                var records = [];
                localStorage.setItem("records", JSON.stringify(records));
            }
            this.currentPage = this.getCurrentPage();
            if(this.currentPage === this.pageCount){
                this.getNotGeted();
            }
        },
        getCurrentPage() {
            if (!localStorage.getItem("currentPage")) {
                localStorage.setItem('currentPage', "0");
                return 0;
            } else {
                return +localStorage.getItem("currentPage");
            }
        },
        getRepMonthList() {
            return [{
                value: '1',
                label: 'январь'
            }, {
                value: 'starter',
                label: 'form.types.starter'
            }, {
                value: 'enterprise',
                label: 'form.types.enterprise'
            }];
        },

        roundValue: roundValue,

        pageCountCalc(recordsCount) {
            const count = recordsCount / Vue.config.formTableRecordsPerPage;
            return count <= 1 ? 1 : Math.ceil(count);
        },

        getValidationField(field) { //Чекаем, есть ли валидация для поля
            if (this.validations[field]) {
                return this.validations[field];
            }
            throw Error(`No validation for field ${field}`);
        },

        pushErr(field) {
            !this.errors.some(el => el.field === field) ? this.errors.push({'field': field}) : null
        },
        getFieldClasses(field) {
            return {'is-invalid': this.isErrorField(field)}
        },
        removeError(field) {
            this.errors = this.errors.filter(el => el.field !== field);
        },
        validateField(field) {
            try {
                this.getValidationField(field);
                if (isRequiredError(this.form[field])) { //Чекаем, пустое ли поле
                    // Если еще нет в массиве ошибок - пушим туда
                    this.pushErr(field)
                } else {
                    //Вырезаем из массива ошибок
                    this.removeError(field)
                }
            } catch (error) {
            }
        },
        onFieldInput(event) {
            const input = event.srcElement;
            const field = input.id;
            this.validateField(field)
        },
        onFileInput(event) {
            this.isFileUploadError ? this.isFileUploadError = false: null;
            let formData = new FormData();
            formData.append('file', this.$refs.fileInput.files[0]);
            axios.post(Vue.config.formFile, formData).then(response => {
                const id = this.files.push(response.data.id);
                console.log(response.data);
                this.fileInputHandler();
            }).catch(error => {
                this.isFileUploadError = true;
            });
        },
        fileInputHandler() {
            if (this.$refs.fileInput.files[0]) {
                this.fileName = this.$refs.fileInput.files[0].name;
                this.removeError("file");
            } else {
                this.pushErr("file")
            }
        },
        isErrorField(field) {
            return this.errors.some(el => el.field === field);
        },

        makeTableItems(responseResult, page) {
            responseResult.forEach((element, index) => {
                element.index = index + Vue.config.formTableRecordsPerPage * page;
                element.page = page;

                let storedRecords = JSON.parse(localStorage.getItem("records"));
                const isStored = storedRecords.some(el => el.id === element.id);

                if (isStored) {
                    const storedRecord = R.find(R.propEq('id', element.id), storedRecords);
                    element.consumptionRate = storedRecord.data;
                }

                element.error = false;
                this.table[`page${page}`].push(element);
            });
        },

        getCurrentPageRecords(page) {
            console.log(`${Vue.config.formApiGet}?count=${Vue.config.formTableRecordsPerPage}&pageId=${page}&sessionId=${this.sessionId}`);
            return axios.get(`${Vue.config.formApiGet}?count=${Vue.config.formTableRecordsPerPage}&pageId=${page}&sessionId=${this.sessionId}`).then((response) => {
                this.recordsCount = +response.data.count;
                this.pageCount = +this.pageCountCalc(response.data.count, Vue.config.formTableRecordsPerPage);
                Vue.set(this.table, `page${page}`, []);
                this.makeTableItems(response.data.result, page);
                this.toggleTotal();
            });
        },

        togglePage() {
            !this.table[`page${this.currentPage}`] ? this.getCurrentPageRecords(this.currentPage) : null;
        },

        onNext() {
            this.currentPage++;
            localStorage.setItem('currentPage', this.currentPage);
            this.togglePage();
            this.toggleTotal();
        },

        onPrev() {
            this.currentPage--;
            localStorage.setItem('currentPage', this.currentPage);
            this.togglePage();
            this.toggleTotal();
        },

        submit() {

            //______________form

            Object.keys(this.form).forEach((field) => {
                this.validateField(field)
            });
            this.fileInputHandler();

            //______________table

            this.tableValidateCurrentPage();
            this.tableValidateAll();


            if (this.errors.length === 0 && this.tableValidateAll()) { // если нет ошибок на форме и все поля таблицы заполнены
                this.getNotGeted().then(() => {
                    this.signFunc();
                });
            }
        },

//___________________________table

        tableValidateCurrentPage() {
            for (var page in this.table) {
                this.table[page].forEach((el) => {
                    el.error = !!isRequiredError(el.consumptionRate);
                });
            }
        },

        prepareDataForSubmit() {
            let formData = new FormData();
            formData.append('file', this.$refs.fileInput.files[0]);
            var data = {};
            data.table = this.allRecords;
            data.form = R.dissoc('file', this.form);
            data.form.files = this.files;
            this.json.Bin = data.form.bin;
            this.json.Phone = data.form.contactPhone;
            this.json.GosOrganId = this.form.orgnameId;
            this.json.Month = parseInt(data.form.repMonth, 10);
            this.json.Year = parseInt(data.form.repYear, 10);
            this.json.StartYearSubsidies = parseFloat(data.form.subsSum);
            this.json.Table = data.table;
            console.log(data.form.files);
            this.json.Files = data.form.files;
            this.json.RegionId = parseInt(data.form.locality, 10);
            this.json.Email = data.form.email;
            return data
        },

        submitFunc(data) {
            //console.log(JSON.stringify(data));
            $.ajax({
                method: "POST",
                url: `${this.ceuProdServiceUrl}/${this.lang}/WaterService/api?sessionId=${this.sessionId}`,
                contentType: 'application/json',
                data: JSON.stringify(data),
                processData: false,
                success: (response) => {
                    console.log(response);
                    if(response.message){
                        showErrorDialog(response.message);
                        return;
                    }
                    showMessageDialog(this.$t('form.success'));
                    setTimeout(() => {document.location = `/${this.lang}/Home/PersonalStatuses`;}, 1000);
                },
                error: () => {
                  showErrorDialog(this.$t('form.fail'));
                }
            });
            // this.isFormSubmitting = true;
            // axios.post(Vue.config.formApiPostMock, this.prepareDataForSubmit()).then(response => {
            //     this.isSubmitted = true;
            //     this.isSubmittedSuccess = true;
            //     this.isFormSubmitting = false;
            // }).catch(error => {
            //     this.isSubmitted = true;
            //     this.isSubmittedError = true;
            //     this.isFormSubmitting = false;
            // });
        },

        signFunc() {
            var signData = this.prepareDataForSubmit();
            this.signData(this.json,
                (res) => {
                this.json.Xml = res;
                this.submitFunc(this.json);
            });
        },

        signData(data, callBack) {
            $.ajax({
                method: "POST",
                url: `/${this.lang}/ServiceDelivery/SignData`,
                contentType: 'application/json',
                data: JSON.stringify({ xml: data }),
                processData: false,
                success: function(response) {
                    if (response.StatusCode !== 200) {
                        showErrorDialog(response.Message);
                        return;
                    }
                    setSignCertXmlWithCertType("SIGNATURE",
                        response.Data,
                        function(res) {
                            callBack(res);
                        });
                },
                error: () => {
                    showErrorDialog(this.$t('form.fail'));
                }
            });
        },

        getContragentInfo(){
            var form = this.form;
            $.ajax({
                method: "GET",
                url: `${this.ceuProdServiceUrl}/${this.lang}/Profile/GetContragentInfo?sessionId=${this.sessionId}`,
                contentType: 'application/json',
                success: (response) => {
                    // console.log(response);
                    // console.log(response.boss);
                    // console.log(form);
                    this.form.headFio = response.boss;
                    this.form.orgname = response.name;
                    this.form.contactPhone = response.phone;
                    this.form.urAddress = response.fullAddress;
                    this.form.email = this.email;
                    this.form.bin = this.iin;
                    this.form.orgnameId = response.id;
                },
                error: () => {
                    showErrorDialog(this.$t('form.fail'));
                }
            });
        },

        getRegionList(){
            var arr = [];
            $.ajax({
                method: "GET",
                url: `${this.ceuProdServiceUrl}/${this.lang}/DirectoryOfRegions/ListForOptions?sessionId=${this.sessionId}`,
                contentType: 'application/json',
                success: (response) => {
                    console.log(response);
                    response.forEach((element) => {
                      arr.push({value: element.id, label: element.name})
                    });
                },
                error: () => {
                    showErrorDialog(this.$t('form.fail'));
                }
            });
          console.log(arr);
          this.localityList = arr;
        },

        getNotGeted() {
            const that = this;
            var need = [];
            for (let i = 0; i < that.pageCount; i++) {
                if (!that.table[`page${i}`]) {
                    need.push(this.getCurrentPageRecords(i));
                }
            }
            return Promise.all(need);
        },

        tableValidateAll() {
            const records = JSON.parse(localStorage.getItem("records"));
            var pagesError = [];
            let isAllPageValid = false;
            if (records) {
                for (var i = 0; i < this.pageCount; i++) {
                    const pageRecords = records.filter(record => record.page === i);
                    const hasErrorInFields = pageRecords.some(el => isRequiredError(el.data));
                    const notLastPage = i !== this.pageCount - 1;
                    const mustRecordValue = notLastPage ? Vue.config.formTableRecordsPerPage : this.recordsCount - Vue.config.formTableRecordsPerPage * i;
                    const isPageValid = !hasErrorInFields && +pageRecords.length === +mustRecordValue;
                    pagesError.push(isPageValid);
                }
                isAllPageValid = pagesError.every((el) => {
                    return el === true
                });
            }
            this.isTablePageError = !isAllPageValid;
            return isAllPageValid;
        },

        storeToLocalStorage(data) {
            let storedRecords = JSON.parse(localStorage.getItem("records"));

            const isStored = storedRecords.some(el => el.id === data.id);

            if (isStored) {
                const storedRecord = R.find(R.propEq('id', data.id), storedRecords);
                storedRecord.id = data.id;
                storedRecord.page = data.page;
                storedRecord.error = data.error;
                storedRecord.data = data.consumptionRate;
            } else {
                storedRecords.push({"id": data.id, "data": data.consumptionRate, "page": data.page, "error": data.error});
            }

            localStorage.setItem('records', JSON.stringify(storedRecords));
        },

        consumptionRateUpdate(data) {
            let record = R.find(R.propEq('id', data.id), this.table[`page${this.currentPage}`]); // берем запись, с которой прилетело событие в общей таблице

            this.storeToLocalStorage(data);

            record.error = !!isRequiredError(data.consumptionRate);

            this.tableValidateAll();
        },
    },
    watch: {
        errors() {
            this.isFormError = this.errors.length > 0;
        },
        localityList: function(newVal){
          console.log(newVal);
        },
    }
}

