import * as R  from 'ramda';
import {roundValue} from "../../helpers/roundValue";

// k = ((Тариф - Размер субсидии)/Тариф)*100
// (Стоимость по тарифу *k) / 100


function isRequiredError(fieldValue) {
    return fieldValue.length === 0 || fieldValue === null;
}

export default ({
    name: 'record',
    props: ['record'],
    data() {
        return {
            consumptionRateError: false,
        }
    },

    watch: {
        'record.consumptionRate': function(newVal){
            this.record.consumptionRate = R.replace(/[^0-9.]/g, '', newVal || '');
            this.$emit('update:consumptionRate',
                {"id": this.record.id, "consumptionRate": newVal, "page": this.record.page, "error": this.record.error});
        },

    },

    methods: {
        roundValue: roundValue,
    },

    computed: {
        localTariffPrice: function () {
            let consumptionRate = +this.record.consumptionRate;
            let subsidyConsumptionRate = +this.record.subsidyConsumptionRate;
            let tariff = +this.record.tariff;

            if(consumptionRate <= subsidyConsumptionRate){ // Если Расход < Водопотребление, подлежащее субсидированию
                return consumptionRate * tariff;
            }else if(consumptionRate > subsidyConsumptionRate){ // Если Расход > Водопотребление, подлежащее субсидированию
                return subsidyConsumptionRate * tariff;
            }
        },

        localSubsidySum: function(){
            const tariff = this.record.tariff;
            const subsidyPrice = this.record.SubsidyPrice;
            const k = (tariff - subsidyPrice)/tariff * 100;
            return localTariffPrice * k / 100;
        },

        total: function(){
            return this.localTariffPrice - this.record.subsidySum
        },

    },
});
