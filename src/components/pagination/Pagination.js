import Vue from "vue";

export default ({
    name: 'pagination',
    props: ['pageCount', 'current', 'tablePageValidate'],
    data() {
        return {
            recordsPerPage : Vue.config.formTableRecordsPerPage,
        }
    },
    computed: {
        currentPage: function() {
            return this.current+1
        }

    },
    methods: {
        prevPage(event) {
            this.$emit('prevPage', this.current);
        },
        nextPage(event) {
            this.$emit('nextPage', this.current);
        },
    }
});
